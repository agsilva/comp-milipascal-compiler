#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvore.h"

extern int yylineno;
extern int pos_col;
extern char* yytext;

No* novoNo(char* tipo, char* texto)
{
	No* novo = (No*)malloc(sizeof(No));
	novo->tipo = strdup(tipo); 
	novo->texto = strdup(texto);// Se não tiver texto fica com a string "" (vazio)
	novo->linha = yylineno;
    novo->coluna =  pos_col-((int)strlen(yytext));
	novo->irmao = NULL;
	novo->filho = NULL;

	return novo;
}

No* verifStat(No* no)
{
	No* aux;
	
	if (!no || no->irmao)
	{
		aux = novoNo("StatList","");
		aux->filho = no;
		
		return aux;
	}
	
	return no;
}

No* insereNo(No* ini, No* fin)
{
	No* no = ini;

	if (ini)
	{
		while(no->irmao)
			no = no->irmao;

		no->irmao = fin;
		
		return ini;
	}
	
	return fin;
}


void imprimePontos(int lvl)
{
	int i;
	for(i = 0; i < lvl; i++)
		printf("..");
}

void imprimeNo(No* no, int lvl)
{
	while (no)
	{
			imprimePontos(lvl);

			if(strcmp(no->texto,"") == 0 || strcmp(no->tipo,"Not") == 0 || strcmp(no->tipo,"Or") == 0 || strcmp(no->tipo,"And") == 0 || strcmp(no->tipo,"Mod") == 0 || strcmp(no->tipo,"Div") == 0)
				printf("%s\n",no->tipo);
			
			else
				printf("%s(%s)\n",no->tipo, no->texto);
			

		imprimeNo(no->filho,lvl + 1);	
		no = no->irmao;
	}
}


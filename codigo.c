#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "arvore.h"
#include "funcoes.h"
#include "codigo.h"

#define SIZE_INT 3
#define SIZE_DOUBLE 6
#define SIZE_FALSE 6
#define SIZE_TRUE 5
#define BUFFER 1024
#define MAX_INT_NUMBERS 13

extern Tabela *symtab;

int indString = 1;
int indVar = 4;
Codigo *listaCodigo;
ValorBool *listaBool;

char *actFunc;

void geraCodigo(No *no, Tabela *tabela)
{
	Elem *aux;
	
	listaCodigo = NULL;
	listaBool = NULL;
	actFunc = (char *)malloc(sizeof(char) * BUFFER);

	declaraInit();
	imprimeStrings(no);
	
	aux = tabela->elem;

	while(aux)
	{
		if(strcmp(aux->tipo, "_function_") == 0)
		{
			strcpy(actFunc, aux->nome);
			novaDeclFunc(aux->nome, no, tabela);		
		}

		else		
			imprimeVarGlobal(aux);

		aux = aux->next;
	}
	
	imprimeTitulo("i32", "main");
	printf("i32 %%paramcount, i8** %%paramstr) {\n");

	printf("  %%1 = alloca i32\n");	
	printf("  %%2 = alloca i32\n");	
	printf("  %%3 = alloca i8**\n");
	printf("  store i32 0, i32* %%1\n");
	printf("  store i32 %%paramcount, i32* %%2\n");
	printf("  store i8** %%paramstr, i8*** %%3\n");

	strcpy(actFunc, "main");

	procuraMain(getMain(no), tabela->elem);

	imprimeReturn("i32", "0");
}

void declaraInit()
{
	printf("; Strings\n");
	printf("@.strNewLine = private unnamed_addr constant [2 x i8] c\"\\0A\\00\"\n");
	printf("@.strInt = private unnamed_addr constant [3 x i8] c\"%%d\\00\"\n");
	printf("@.strDouble = private unnamed_addr constant [6 x i8] c\"%%.12E\\00\"\n");
	printf("@.str = private unnamed_addr constant [3 x i8] c\"%%s\\00\"\n");
	printf("@.strTrue = private unnamed_addr constant [5 x i8] c\"TRUE\\00\"\n");
	printf("@.strFalse = private unnamed_addr constant [6 x i8] c\"FALSE\\00\"\n");
	printf("\n");

	printf("; Declarations\n");
	printf("declare i32 @printf(i8*, ...)\n");
	printf("\n");
}

void imprimeStrings(No *no)
{
	if(no)
	{
		if(strcmp(no->tipo, "String") == 0)
		{
			no->texto = formataString(no->texto);
			insereCodigo(no->texto);			
		}

		imprimeStrings(no->filho);
		imprimeStrings(no->irmao);
	}
}


void imprimeTitulo(char *tipo, char *nome)
{
	printf("\ndefine %s @%s(", tipo, nome);
}

void imprimeReturn(char *tipo, char *name)
{
	printf("  ret %s %s\n}\n", tipo, name);
}

void imprimeVar(char *name, char *tipo)
{
	printf("  %%%s = alloca %s\n", name, tipo);
}

void imprimeWriteLn(int size, char *id, char *tipo, char *texto)
{
	printf("  %%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([%d x i8]* %s, i32 0, i32 0), %s %s)\n", indVar++, size, id, tipo, texto);
}

void imprimeVarGlobal(Elem *el)
{
	char *aux;

	aux = getType(el->tipo);

	if(strcmp(aux, "double") == 0)
		printf("@%s = common global %s 0.0\n", el->nome, aux);

	else
	{
		printf("@%s = common global %s 0\n", el->nome, aux);

		if(strcmp(aux, "i1") == 0)
			insereValor(el->nome, 0, "main");
	}	
}

char *formataString(char *str)
{
	int i, j;
	char *s = (char *)malloc(sizeof(char) * (strlen(str) - 2));
	
	for(i = 1, j = 0; i <= strlen(str) - 2; i++, j++)
	{
		s[j] = str[i];
		if(str[i] == '\'' && i + 1 <= strlen(str) - 2 && str[i + 1] == '\'')
			i++;
	}

	return s;
}

void novaDeclFunc(char *nome, No *no, Tabela *tabela)
{
	Tabela *tab;

	nome = toLower(nome);
	tab = getFunc(nome);
	
	imprimeTitulo(getType(tab->elem->tipo), nome);
	
	procuraVar(getParam(tab->elem->next), tabela->elem->nome);
	procuraMain(procuraFunc(no, nome), tab->elem);
						
	imprimeReturn(getType(tab->elem->tipo), "0");
							
}

void insereCodigo(char *texto)
{
	Codigo *novo, *aux;

	if (listaCodigo)
	{
		aux = listaCodigo;
		novo = (Codigo *)malloc(sizeof(Codigo));

		if(strcmp(texto, aux->linha) == 0)
			return;

		while(aux->next){
			if(strcmp(texto, aux->linha) == 0)
				return;

			aux = aux->next;
		}

		novo->linha = texto;
		novo->id = indString;
		aux->next = novo;
	}
	
	else
	{
		listaCodigo = (Codigo*)malloc(sizeof(Codigo));
		listaCodigo->linha = texto;
		listaCodigo->id = indString;
	}		

	printf("@.str%d = private unnamed_addr constant [%d x i8] c\"%s\\00\"\n", indString++, (int)strlen(texto) + 1, texto);
}

void procuraVar(Elem *el, char *function)
{
	Elem *aux;

	aux = el;

	while(aux)
	{
		imprimeVar(aux->nome, getType(aux->tipo));

		if(strcmp(aux->tipo, "_boolean_") == 0)
			insereValor(aux->nome, 0, function);

		aux = aux->next;
	}
}

No *procuraFunc(No *no, char *name)
{
	No *aux;

	aux = no->filho;
	name = toLower(name);
				
	while(strcmp(aux->tipo, "FuncPart") != 0)
		aux = aux->irmao;
	
	aux = aux->filho;
	while(strcmp(toLower(aux->filho->texto), name) != 0)
		aux = aux->irmao;

	return aux->filho->irmao;
}

Elem *getParam(Elem *el)
{
	Elem *aux;

	if(!el)
	{
		printf(") {\n");
		return NULL;
	}

	aux = el;

	while(1)
	{
		if(aux->flag)
			printf("%s %%%s", getType(aux->tipo), aux->nome);

		aux = aux->next;

		if(aux && aux->flag)
			printf(", ");

		else
		{
			printf(") {\n");
			break;
		}
	}

	return aux;
}


char *getType(char *tipo)
{
	if(strcmp(tipo, "_integer_") == 0)
		return "i32";

	if(strcmp(tipo, "_boolean_") == 0)
		return "i1";

	return "double";
}

int getID(char *texto)
{
	Codigo *aux;

	aux = listaCodigo;

	while(strcmp(texto, aux->linha) != 0){
		aux = aux->next;
	}

	return aux->id;
}

No *getMain(No *no)
{
	No *aux;

	aux = no->filho;

	while(strcmp(aux->tipo, "FuncPart") != 0)
		aux = aux->irmao;

	return aux;
}

void procuraMain(No *no, Elem *el)
{
	Elem *elem2, *line2;
	char *aux, *aux2;
	char *str;

	aux = (char*) malloc(sizeof(char) * BUFFER);
	aux2 = (char*) malloc(sizeof(char) * BUFFER);

	if(no)
	{
		if(strcmp(no->tipo, "WriteLn") == 0)
			procuraWriteLn(no, el);			

		else if(strcmp(no->tipo, "Assign") == 0){
			str = rAssign(no->filho->irmao, el);
			
			elem2 = getElemGlobal(toLower(no->filho->texto));			
			strcpy(aux, "@");

			if(!elem2){
				elem2 = el;
				
				while(elem2 && strcmp(toLower(no->filho->texto), elem2->nome) != 0)
					elem2 = elem2->next;				

				strcpy(aux, "%");
			}

			strcat(aux, toLower(no->filho->texto));

			if(strcmp(elem2->tipo, "_real_") == 0 && !strchr(str, '.')){
				if(strchr(str, 'e'))
					str = insereChar(str);

				else
					strcat(str, ".0");
			}			

			if(strcmp(no->filho->irmao->tipo, "Id") == 0){
				if(strcmp(toLower(no->filho->irmao->texto), "false") == 0){
					if(!substituiValor(no->filho->texto, 0, actFunc))
						substituiValor(no->filho->texto, 0, "main");

					printf("  %%%d = load %s* %s\n", indVar++, getType(elem2->tipo), rAssign(no->filho, elem2));
					printf("  store %s %s, %s* %s\n", getType(elem2->tipo), str, getType(elem2->tipo), aux);
				}

				else if(strcmp(toLower(no->filho->irmao->texto), "true") == 0){
					if(!substituiValor(no->filho->texto, 1, actFunc))
						substituiValor(no->filho->texto, 1, "main");

					printf("  %%%d = load %s* %s\n", indVar++, getType(elem2->tipo), rAssign(no->filho, elem2));
					printf("  store %s %s, %s* %s\n", getType(elem2->tipo), str, getType(elem2->tipo), aux);
				}
				
				else{
					if(strcmp(elem2->tipo, "_boolean_") == 0){
						if(procuraValor(no->filho->irmao->texto, actFunc) == -1){
							if(!substituiValor(no->filho->texto, procuraValor(no->filho->irmao->texto, "main"), actFunc))
								substituiValor(no->filho->texto, procuraValor(no->filho->irmao->texto, "main"), "main");					
						}

						if(!substituiValor(no->filho->texto, procuraValor(no->filho->irmao->texto, actFunc), actFunc)){
							substituiValor(no->filho->texto, procuraValor(no->filho->irmao->texto, actFunc), "main");
						}
					}

					line2 = getElemGlobal(toLower(no->filho->irmao->texto));
					strcpy(aux2, "@");

					if(!line2){
						line2 = el;
						
						while(line2 && strcmp(toLower(no->filho->irmao->texto), line2->nome) != 0)
							line2 = line2->next;
						
						strcpy(aux2, "%");
					}

					strcat(aux2, toLower(no->filho->irmao->texto));

					if(strcmp(line2->tipo, "_integer_") == 0 && strcmp(elem2->tipo, "_real_") == 0){					
						printf("  %%%d = load %s* %s\n", indVar++, getType(line2->tipo), rAssign(no->filho->irmao, line2));
						printf("  %%%d = sitofp i32 %%%d to double\n", indVar, indVar - 1);
						++indVar;
					}

					else
						printf("  %%%d = load %s* %s\n", indVar++, getType(line2->tipo), aux2);

					printf("  store %s %%%d, %s* %s\n", getType(elem2->tipo), indVar - 1, getType(elem2->tipo), aux);
				}
			}

			else
				printf("  store %s %s, %s* %s\n", getType(elem2->tipo), str, getType(elem2->tipo), aux);
		}

		procuraMain(no->filho, el);
		procuraMain(no->irmao, el);
	}
}
void procuraWriteLn(No *no, Elem *el)
{
	No *aux;
	char temp[MAX_INT_NUMBERS], *str;
	Elem *elem2;

	aux = no;
	str = (char *)malloc(sizeof(char) * BUFFER);
	
	while(aux->filho)
	{
		if(strcmp(aux->filho->tipo, "String") == 0)
			printf("  %%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([%d x i8]* @.str%d, i32 0, i32 0))\n", indVar++, (int)strlen(aux->filho->texto) + 1, getID(aux->filho->texto));

		else if(strcmp(aux->filho->tipo, "IntLit") == 0)
			imprimeWriteLn(SIZE_INT, "@.strInt", "i32", aux->filho->texto);

		else if(strcmp(aux->filho->tipo, "RealLit") == 0){
			if(!strchr(aux->filho->texto, '.')){
				if(strchr(aux->filho->texto, 'e'))
					aux->filho->texto = insereChar(aux->filho->texto);

				else
					strcat(aux->filho->texto, ".0");
			}

			imprimeWriteLn(SIZE_DOUBLE, "@.strDouble", "double", aux->filho->texto);
		}

		else if(strcmp(toLower(aux->filho->texto), "true") == 0)
			imprimeWriteLn(SIZE_TRUE, "@.strTrue", "i1", aux->filho->texto);
		
		else if(strcmp(toLower(aux->filho->texto), "false") == 0)
			imprimeWriteLn(SIZE_FALSE, "@.strFalse", "i1", aux->filho->texto);

		else if(strcmp(aux->filho->tipo, "Id") == 0){
			if(strcmp(toLower(aux->filho->texto), "paramcount") == 0)
				imprimeWriteLn(SIZE_INT, "@.strInt", "i32", "%paramcount");

			else{
				elem2 = getElemGlobal(toLower(aux->filho->texto));
				strcpy(str, "@");

				if(elem2 == NULL){
					elem2 = el;
					
					while(elem2 != NULL && strcmp(toLower(aux->filho->texto), elem2->nome) != 0){
						elem2 = elem2->next;
					}
					
					strcpy(str, "%");
				}

				strcat(str, toLower(aux->filho->texto));			

				printf("  %%%d = load %s* %s\n", indVar, getType(elem2->tipo), str);

				sprintf(temp, "%%%d", indVar);
				++indVar;

				if(strcmp(elem2->tipo, "_integer_") == 0)
					imprimeWriteLn(SIZE_INT, "@.strInt", "i32", temp);

				else if(strcmp(elem2->tipo, "_boolean_") == 0)
				{
					
					if(procuraValor(aux->filho->texto, actFunc))
						imprimeWriteLn(SIZE_TRUE, "@.strTrue", "i1", temp);	

					else
						imprimeWriteLn(SIZE_FALSE, "@.strFalse", "i1", temp);
				}

				else
					imprimeWriteLn(SIZE_DOUBLE, "@.strDouble", "double", temp);
				
			}
		}

		else
			str = rAssign(aux->filho, el);

		aux->filho = aux->filho->irmao;		
	}

	printf("  %%%d = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([2 x i8]* @.strNewLine, i32 0, i32 0))\n", indVar++);
}



char *rAssign(No *no, Elem *el)
{
	Elem *elem2;
	char *aux;	

	aux = (char*) malloc(sizeof(char) * BUFFER);

	if(strcmp(no->tipo, "IntLit") == 0 || strcmp(no->tipo, "RealLit") == 0) 
		return no->texto;

	if(strcmp(toLower(no->texto), "true") == 0)
		return "1";

	if(strcmp(toLower(no->texto), "false") == 0)
		return "0";

	if(strcmp(no->tipo, "Id") == 0)
	{
		elem2 = getElemGlobal(toLower(no->texto));
		strcpy(aux, "@");

		if(!elem2)
		{
			elem2 = el;
			
			while(elem2 != NULL && strcmp(toLower(no->texto), elem2->nome) != 0)
				elem2 = elem2->next;
			
			strcpy(aux, "%");
		}		
		
		strcat(aux, toLower(no->texto));
		
		return aux;
	}

	return "";
}

int verifTerminal(char* tipo)
{
	if(strcmp(tipo, "IntLit") == 0 || strcmp(tipo, "RealLit") == 0 || strcmp(tipo, "Id") == 0)
		return 0;

	return 1;
}

Elem *getElemGlobal(char *nome)
{
	Elem *el;

	nome = toLower(nome);
	
	el = symtab->elem;

	while(el && strcmp(el->tipo,"_function")!=0)
	{
		if(strcmp(nome, el->nome) == 0)
			return el;
		
		
		el = el->next;
	}

	return NULL;
}

int procuraValor(char *nome, char *function)
{
	ValorBool *aux;

	nome = toLower(nome);

	aux = listaBool;

	while(aux)
	{
		if(strcmp(nome, aux->nome) == 0 && strcmp(function, aux->function) == 0)
			return aux->valor;

		aux = aux->next;
	}

	return -1;
}

int substituiValor(char *nome, int valor, char *function)
{
	ValorBool *aux;

	nome = toLower(nome);

	aux = listaBool;

	while(aux)
	{
		if(strcmp(nome, aux->nome) == 0 && strcmp(function, aux->function) == 0)
		{
			aux->valor = valor;
			return 1;
		}

		aux = aux->next;
	}

	return 0;
}


void insereValor(char *nome, int valor, char *function)
{
	ValorBool *novo, *aux;

	if (listaBool)
	{
		aux = listaBool;
		novo = (ValorBool *)malloc(sizeof(ValorBool));

		while(aux->next != NULL)
			aux = aux->next;

		novo->valor = valor;
		novo->nome = nome;
		novo->function = function;
		aux->next = novo;
	}

	else
	{
		listaBool = (ValorBool *)malloc(sizeof (ValorBool));
		listaBool->valor = valor;
		listaBool->nome = nome;
		listaBool->function = function;
	}
}

char *insereChar(char *str)
{
	int i, j;
	char *aux;

	aux = (char *)malloc(sizeof(char) * strlen(str) + 2);

	for(i = j = 0; i < strlen(str); ++i, ++j)
	{
		if(str[i] == 'e')
		{
			aux[j++] = '.';
			aux[j++] = '0';
			aux[j] = 'e';
		}

		else
			aux[j] = str[i];
	}

	return aux;
}


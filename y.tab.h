/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    INTLIT = 258,
    REALLIT = 259,
    STRING = 260,
    DOT = 261,
    COLON = 262,
    COMMA = 263,
    SEMIC = 264,
    ASSIGN = 265,
    BEGINO = 266,
    DO = 267,
    ELSE = 268,
    END = 269,
    FORWARD = 270,
    FUNCTION = 271,
    ID = 272,
    IF = 273,
    OUTPUT = 274,
    PARAMSTR = 275,
    PROGRAM = 276,
    REPEAT = 277,
    UNTIL = 278,
    VAL = 279,
    VAR = 280,
    WHILE = 281,
    WRITELN = 282,
    RESERVED = 283,
    THEN = 284,
    LT = 285,
    LEQ = 286,
    GT = 287,
    GEQ = 288,
    EQ = 289,
    NEQ = 290,
    ADD = 291,
    SUB = 292,
    MUL = 293,
    REALDIV = 294,
    OR = 295,
    MOD = 296,
    DIV = 297,
    AND = 298,
    NOT = 299,
    LBRAC = 300,
    RBRAC = 301
  };
#endif
/* Tokens.  */
#define INTLIT 258
#define REALLIT 259
#define STRING 260
#define DOT 261
#define COLON 262
#define COMMA 263
#define SEMIC 264
#define ASSIGN 265
#define BEGINO 266
#define DO 267
#define ELSE 268
#define END 269
#define FORWARD 270
#define FUNCTION 271
#define ID 272
#define IF 273
#define OUTPUT 274
#define PARAMSTR 275
#define PROGRAM 276
#define REPEAT 277
#define UNTIL 278
#define VAL 279
#define VAR 280
#define WHILE 281
#define WRITELN 282
#define RESERVED 283
#define THEN 284
#define LT 285
#define LEQ 286
#define GT 287
#define GEQ 288
#define EQ 289
#define NEQ 290
#define ADD 291
#define SUB 292
#define MUL 293
#define REALDIV 294
#define OR 295
#define MOD 296
#define DIV 297
#define AND 298
#define NOT 299
#define LBRAC 300
#define RBRAC 301

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 19 "mpacompiler.y" /* yacc.c:1909  */

	char* str;
	struct no* no;

#line 151 "y.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

#include "arvore.h"

void geraCodigo(No *no, Tabela *tabela);
void declaraInit();
void imprimeStrings(No *no);

void imprimeTitulo(char *tipo, char *nome);
void imprimeReturn(char *tipo, char *name);
void imprimeVar(char *name, char *tipo);
void imprimeWriteLn(int size, char *id, char *tipo, char *texto);
void imprimeVarGlobal(Elem *el);

char *formataString(char *str);
void novaDeclFunc(char *nome, No *no, Tabela *tabela);
void insereCodigo(char *texto);
void procuraVar(Elem *el, char *function);
No *procuraFunc(No *no, char *name);

Elem *getParam(Elem *el);
char *getType(char *tipo);
int getID(char *texto);
No *getMain(No *no);

void procuraMain(No *no, Elem *el);
void procuraWriteLn(No *no, Elem *el);
char *rAssign(No *no, Elem *el);

int verifTerminal(char* tipo);

Elem *getElemGlobal(char *nome);
int procuraValor(char *nome, char *function);
int substituiValor(char *nome, int valor, char *function);
void insereValor(char *nome, int valor, char *function);
char *insereChar(char *str);

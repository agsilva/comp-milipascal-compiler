#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "funcoes.h"
#include "semantica.h"

extern Tabela *symtab;

char* toLower(char* str)
{
	int i;
	char* novaStr = (char*)malloc(((int)strlen(str)+1) * sizeof(char));
	
	for(i = 0 ; i <= strlen(str) ; i++)
		novaStr[i] = tolower(str[i]);
			
	return novaStr;
}

void addFunc(char* name)
{
	insereElem(symtab->next->next, NULL, name, "_function_",NULL,NULL);
}

Tabela* getFunc(char* name)
{
	Tabela* tab = symtab->next->next;
	
	if (name)
	{
		name = toLower(name);
		while(tab)
		{
			if (tab->elem && strcmp(tab->elem->nome, name)==0 && tab->elem->flag && strcmp(tab->elem->flag, "return")==0)
				return tab;
			
			tab = tab->next;
		}
	}
	
	return NULL;
}

int getFuncNum(Tabela* tabela)
{
	Elem *el;
	
	int count = 0;
	
	for (el = tabela->elem->next; el!=NULL && el->flag; el = el->next)
		count++;
	
	return count;
}

int getCallNum(No* no)
{
	No *aux;
	
	int count = 0;
	
	for (aux = no->filho->irmao; aux!=NULL; aux= aux->irmao)
		count++;
	
	return count;
}

No* getVarTipo(No *no)
{	
    while(no->irmao)
        no = no->irmao;
        
    return no;
}

Elem *getElem(No *no, Tabela *tabela)
{
	Elem *el;
	char *str = toLower(no->texto);
	
	if (tabela)
		for (el = tabela->elem; el != NULL; el = el->next)
			if (strcmp(el->nome,str) == 0)
				return el;		
	
	for (el = symtab->next->next->elem; el!=NULL; el=el->next)
		if (strcmp(el->nome,str) == 0)
			return el;
		
	for (el = symtab->elem; el!=NULL; el=el->next)
		if (strcmp(el->nome,str) == 0)
			return el;	
	
	/* 9 - Symbol <token> not defined */
	imprimeErro(9, no->linha,no->coluna, no->texto,NULL,NULL,NULL);
	
	return NULL;
}

Elem *verifElem(No *no, Tabela *tabela)
{
	Elem *el;
	char *str = toLower(no->texto);
	
	if (tabela){
		for (el = tabela->elem; el != NULL; el = el->next)
			if (strcmp(el->nome,str) == 0)
				return el;
	}		
	
	for (el = symtab->next->next->elem; el!=NULL; el=el->next)
		if (strcmp(el->nome,str) == 0)
			return el;
		
	return NULL;
}


char* verifVarTipo(No *no, Tabela *tabela)
{
	Elem *el;
	
	el = getElem(no,tabela);
	
	if (strcmp(el->tipo,"_type_") != 0)
		imprimeErro(10, no->linha,no->coluna,NULL,NULL,NULL,NULL); 	/*Type identifier expected*/

	return el->valor;
}

char* verifVarDecl(No *no, Tabela *tabela)
{
	Elem *el;
	Tabela *tab;
	int n_args;
	char num[50];
		
	el = getElem(no,tabela);
	
	if (strcmp(el->tipo,"_function_") == 0)
	{
		tab = getFunc(no->texto);
		
		if(!tab && strcmp("paramcount",el->nome) == 0)			
			return "_integer_";
			
		if ((n_args = getFuncNum(tab)) == 0)
			return tab->elem->tipo;
		
		sprintf(num,"%d",n_args);
			
		/*Wrong number of arguments in call to function <token> (got <number>, expected <number>)*/
		imprimeErro(12,no->linha, no->coluna, no->texto, num,NULL,NULL);
	}

	return el->tipo;
}

int verifType(char *tipo)
{
	
	if (strcmp(tipo, "_boolean_") == 0)
		return 1;
	
	if (strcmp(tipo, "_integer_") == 0)
		return 1;
		
	if (strcmp(tipo, "_real_") == 0)
		return 1;

	return 0;
	
}

char *verifOp(No *no)
{
	if (strcmp (no->tipo, "Add") == 0 || strcmp (no->tipo, "Plus") == 0)
		return "+";
	else if (strcmp (no->tipo, "Sub") == 0 || strcmp (no->tipo, "Minus") == 0)
		return "-";
	else if (strcmp (no->tipo, "Lt") == 0)
		return "<";
	else if (strcmp (no->tipo, "Leq") == 0)
		return "<=";
	else if (strcmp (no->tipo, "Gt") == 0)
		return ">";
	else if (strcmp (no->tipo, "Geq") == 0)
		return ">=";
	else if (strcmp (no->tipo, "Eq") == 0)
		return "=";
	else if (strcmp (no->tipo, "Neq") == 0)
		return "<>";
	else if (strcmp (no->tipo, "Or") == 0 || strcmp(no->tipo, "And") == 0)
		return no->texto;
	else if (strcmp (no->tipo, "Mod") == 0 || strcmp(no->tipo, "Div") == 0)
		return no->texto;
	else if (strcmp (no->tipo, "Not") == 0)
		return no->texto;
	else if (strcmp (no->tipo, "RealDiv") == 0)
		return "/";
	else if (strcmp (no->tipo, "Mul") == 0)
		return "*";

	return NULL;
}

char *verifCall(No *callNo, Tabela *tabela)
{
	Tabela* tab;
	Elem * id;
	int i, varsF, varsC;
	char *tipo;
	char num[50],num2[50];
	
	Elem* paramTab;
	No* paramCall;
	
	id = getElem(callNo->filho,tabela);
	
	if(strcmp(id->tipo, "_function_") == 0 || (id->flag && strcmp(id->flag, "return") == 0))
	{
		if(strcmp(id->nome,"paramcount") == 0 && getFunc(id->nome) == NULL)
			varsF = 0;
		else
		{	
			tab = getFunc(id->nome);
			varsF = getFuncNum(tab);
		}
		varsC = getCallNum(callNo);

		if(varsF != varsC)
		{
			sprintf(num,"%d",varsC);
			sprintf(num2,"%d",varsF);

			/*Wrong number of arguments in call to function <token> (got <number>, expected <number>)*/
			imprimeErro(12,callNo->filho->linha,callNo->filho->coluna,callNo->filho->texto,num,num2,NULL); 
		}

		paramTab = tab->elem->next;
		paramCall = callNo->filho->irmao;

		for(i = 1; i <= varsF && paramTab != NULL && paramCall != NULL; i++, paramTab = paramTab->next, paramCall = paramCall->irmao)
		{
			if(strcmp(paramTab->flag, "varparam") == 0)
			{
				if(strcmp(paramCall->tipo,"Id") == 0)
				{															
					tipo = verifStatPart(paramCall,tabela);
					if(strcmp(paramTab->tipo, tipo) != 0)
					{	
						if(strcmp(tipo, "_type_") == 0) 
							imprimeErro(11, paramCall->linha, paramCall->coluna, NULL, NULL, NULL, NULL);  /*Variable identifier expected*/					
						
						sprintf(num,"%d",i);
						
						/*Incompatible type for argument <num> in call to function <token> (got <type>, expected <type>)*/
						imprimeErro(3,paramCall->linha,paramCall->coluna,num,callNo->filho->texto,tipo,paramTab->tipo);
					}
				}
				else
					imprimeErro(11,paramCall->linha,paramCall->coluna,NULL,NULL,NULL,NULL); /*Variable identifier expected*/
			}
			else 
			{
				tipo = verifStatPart(paramCall,tabela);
				
				if(strcmp(paramTab->tipo, tipo) != 0)  
					if (strcmp(paramTab->tipo,"_real_") != 0 || strcmp(tipo,"_integer_") != 0)
					{
						sprintf(num,"%d",i);		
						/*Incompatible type for argument <num> in call to function <token> (got <type>, expected <type>)*/
						imprimeErro(3,paramCall->linha,paramCall->coluna,num,callNo->filho->texto,tipo,paramTab->tipo);
					}
			}

		}
		
	}
	else
		imprimeErro(2,callNo->filho->linha,callNo->filho->coluna,NULL,NULL,NULL,NULL); /*Function identifier expected*/
		
		
	callNo->linha = callNo->filho->linha;
	callNo->coluna = callNo->filho->coluna;
	
	return tab->elem->tipo;
}


void imprimeErro(int num, int lin, int col, char *str1, char *str2, char *str3, char *str4)
{
	
	/*** 1 - Cannot write values of type <type>
		 2 - Function identifier expected
		 3 - Incompatible type for argument <num> in call to function <token> (got <type>, expected <type>)
		 4 - Incompatible type in assigment to <token> (got <type>, expected <type>)
		 5 - Incompatible type in <statement> statement (got <type>, expected <type>)
		 6 - Operator <token> cannot be applied to type <type>
		 7 - Operator <token> cannot be applied to types <type>, <type>
		 8 - Symbol <token> already defined
		 9 - Symbol <token> not defined
		 10 - Type identifier expected
		 11 - Variable identifier expected
		 12 - Wrong number of arguments in call to function <token> (got <number>, expected <number>)
	***/
	
	switch (num)
	{
		case 1: printf("Line %d, col %d: Cannot write values of type %s\n",lin,col,str1); break;
		case 2: printf("Line %d, col %d: Function identifier expected\n",lin,col); break;
		case 3: printf("Line %d, col %d: Incompatible type for argument %s in call to function %s (got %s, expected %s)\n",lin,col,str1,str2,str3,str4); break;
		case 4: printf("Line %d, col %d: Incompatible type in assigment to %s (got %s, expected %s)\n",lin,col,str1,str2,str3); break;
		case 5: printf("Line %d, col %d: Incompatible type in %s statement (got %s, expected %s)\n",lin,col,str1,str2,str3); break;
		case 6: printf("Line %d, col %d: Operator %s cannot be applied to type %s\n",lin,col,str1,str2); break;
		case 7: printf("Line %d, col %d: Operator %s cannot be applied to types %s, %s\n",lin,col,str1,str2,str3); break;
		case 8: printf("Line %d, col %d: Symbol %s already defined\n",lin,col,str1); break;
		case 9: printf("Line %d, col %d: Symbol %s not defined\n",lin,col,str1); break;
		case 10: printf("Line %d, col %d: Type identifier expected\n",lin,col); break;
		case 11: printf("Line %d, col %d: Variable identifier expected\n",lin,col); break;
		case 12: printf("Line %d, col %d: Wrong number of arguments in call to function %s (got %s, expected %s)\n",lin,col,str1,str2,str3); break;
	}
		
	exit(0);
}


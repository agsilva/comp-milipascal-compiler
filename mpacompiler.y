%{

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "arvore.h"
	#include "tabela_sym.h"

	extern int yylineno, yyleng;
	extern int pos_col;
	extern char* yytext;

	No* arvore = NULL;
	Tabela *symtab = NULL;

	int yyerror(char *s);
%}

%union{
	char* str;
	struct no* no;
}

%token <str> INTLIT
%token <str> REALLIT
%token <str> STRING

%token DOT
%token COLON
%token COMMA
%token SEMIC

%token ASSIGN
%token BEGINO
%token DO
%token ELSE
%token END
%token FORWARD
%token FUNCTION
%token <str> ID
%token IF
%token OUTPUT
%token PARAMSTR
%token PROGRAM
%token REPEAT
%token UNTIL
%token VAL
%token VAR
%token WHILE
%token WRITELN
%token <str> RESERVED

%right ELSE THEN
%nonassoc LT LEQ GT GEQ EQ NEQ
%left ADD SUB MUL REALDIV
%left  <str> OR MOD DIV AND
%right <str> NOT
%left LBRAC RBRAC

%type <no> Prog ProHeading Id ProgBlock VarPart FuncPart StatPart VarDeclaration OptVarDecl  
%type <no> FuncHeading FormalParamList FormalParams OptFormalParams FuncBlock IDList
%type <no> StatList Stat OptStat WritelnPList ExpStr OptPList  
%type <no> Expr RelOp SimpleExpr Sign AddOp Term MultOp Factor Not ParamList

%%

Prog 				: ProHeading SEMIC ProgBlock DOT 					{$$ = arvore = novoNo("Program",""); $$->filho = $1; $1->irmao = $3;}
;

ProHeading 			: PROGRAM Id LBRAC OUTPUT RBRAC						{$$ = $2;}
;

ProgBlock 			: VarPart FuncPart StatPart 						{$$ = novoNo("VarPart",""); $$->filho = $1; $$->irmao = novoNo("FuncPart",""); $$->irmao->filho = $2; $$->irmao->irmao = verifStat($3);}
;

VarPart 			: 													{$$ = NULL;}
					| VAR OptVarDecl									{$$ = $2;}
;

OptVarDecl			: VarDeclaration SEMIC								{$$ = novoNo("VarDecl",""); $$->filho = $1;}
					| VarDeclaration SEMIC OptVarDecl					{$$ = novoNo("VarDecl",""); $$->filho = $1; $$->irmao = $3;}
;

VarDeclaration		: IDList COLON Id 									{$$ = insereNo($1,$3);}
;

IDList 				: Id 												{$$ = $1;}
					| Id COMMA IDList 									{$$ = $1; $1->irmao = $3;}
;

FuncPart 			: 													{$$ = NULL;}
					| FuncHeading SEMIC FORWARD SEMIC FuncPart 			{$$ = novoNo("FuncDecl",""); $$->filho = $1; $$->irmao = $5;}
					| FuncHeading SEMIC FuncBlock SEMIC FuncPart		{$$ = novoNo("FuncDef",""); $$->filho = insereNo($1,$3); $$->irmao = $5;}
					| FUNCTION Id SEMIC FuncBlock SEMIC FuncPart	 	{$$ = novoNo("FuncDef2",""); $$->filho = $2; $2->irmao = $4; $$->irmao = $6;}					  
;

FuncHeading 		: FUNCTION Id FormalParamList COLON Id 				{$$ = $2; $$->irmao = novoNo("FuncParams",""); $$->irmao->filho = $3; $$->irmao->irmao = $5;}
					| FUNCTION Id COLON Id 								{$$ = $2; $$->irmao = novoNo("FuncParams",""); $$->irmao->irmao = $4;}
;

FormalParamList 	: LBRAC FormalParams OptFormalParams RBRAC			{$$ = $2; $2->irmao = $3;}
;

OptFormalParams:														{$$=NULL;}
					| SEMIC FormalParams OptFormalParams				{$$ = $2; $2->irmao = $3;}
;

FormalParams 		: VarDeclaration									{$$ = novoNo("Params",""); $$->filho = $1;}					
					| VAR VarDeclaration								{$$ = novoNo("VarParams",""); $$->filho = $2;}
;

FuncBlock 			: VarPart StatPart									{$$ = novoNo("VarPart",""); $$->filho = $1; $$->irmao = verifStat($2);}
;

StatPart 			: BEGINO StatList END								{if ($2 && $2->irmao) {$$ = novoNo("StatList",""); $$->filho = $2;} else $$ = $2;}
;

StatList 			: Stat OptStat 										{$$ = $1; $$ = insereNo($1,$2);}
;

OptStat				: 													{$$ = NULL;}
					| SEMIC Stat OptStat 								{$$ = $2; $$ = insereNo($2,$3);}
;

Stat 				: 													{$$ = NULL;}
					| StatPart											{$$ = $1;}
					| IF Expr THEN Stat 								{$$ = novoNo("IfElse",""); $$->filho = $2; $2->irmao = verifStat($4); $2->irmao->irmao = novoNo("StatList","");}
					| IF Expr THEN Stat ELSE Stat 			         	{$$ = novoNo("IfElse",""); $$->filho = $2; $2->irmao = verifStat($4); $2->irmao->irmao = verifStat($6);}
				    | WHILE Expr DO Stat 								{$$ = novoNo("While",""); $$->filho = $2; $2->irmao = verifStat($4);}
					| REPEAT StatList UNTIL Expr 						{$$ = novoNo("Repeat",""); $$->filho = verifStat($2); $$->filho->irmao = $4;}
					| VAL LBRAC PARAMSTR LBRAC Expr RBRAC COMMA Id RBRAC{$$ = novoNo("ValParam",""); $$->filho = $5; $5->irmao = $8;}
					| Id ASSIGN Expr 									{$$ = novoNo("Assign",""); $$->filho = $1; $1->irmao = $3;}				
					| WRITELN 											{$$ = novoNo("WriteLn","");}					
					| WRITELN WritelnPList 								{$$ = novoNo("WriteLn",""); $$->filho = $2;}
;

WritelnPList 		: LBRAC ExpStr OptPList RBRAC 						{$$ = $2; $$->irmao = $3;}
;

ExpStr 				: Expr 												{$$ = $1;}
					| STRING   											{$$ = novoNo("String",$1);}
;

OptPList 			:													{$$ = NULL;}
					| COMMA ExpStr OptPList 							{$$ = $2; $$->irmao = $3;}	
;

Expr 				: SimpleExpr										{$$ = $1;}
					| SimpleExpr RelOp SimpleExpr 						{$$ = $2; $$->filho = $1; $1->irmao = $3;}
;

RelOp				: LT 												{$$ = novoNo("Lt","");}
					| LEQ  												{$$ = novoNo("Leq","");}
					| GT 												{$$ = novoNo("Gt","");}
					| GEQ   											{$$ = novoNo("Geq","");}
					| EQ  												{$$ = novoNo("Eq","");}
					| NEQ  												{$$ = novoNo("Neq","");}


SimpleExpr 			: Term 												{$$ = $1;}
 					| Sign Term 										{$$ = $1; $$->filho = $2;}
 					| SimpleExpr AddOp Term 							{$$ = $2; $$->filho = $1; $1->irmao = $3;}
;

Sign 				: ADD 												{$$ = novoNo("Plus","");}
					| SUB 												{$$ = novoNo("Minus","");}

AddOp 				: ADD 												{$$ = novoNo("Add","");}
					| SUB  												{$$ = novoNo("Sub","");}
					| OR  												{$$ = novoNo("Or",$1);}

Term	 			: Factor											{$$ = $1;}
					| Term MultOp Factor								{$$ = $2; $$->filho = $1; $1->irmao = $3;}
;

MultOp 				: AND 												{$$ = novoNo("And",$1);}
					| DIV 												{$$ = novoNo("Div",$1);}
					| REALDIV 											{$$ = novoNo("RealDiv","");}
					| MOD   											{$$ = novoNo("Mod",$1);}
					| MUL  												{$$ = novoNo("Mul","");}

Factor				: Not Factor 										{$$ = $1; $1->filho = $2;}
					| LBRAC Expr RBRAC 									{$$ = $2;}
					| INTLIT											{$$ = novoNo("IntLit",$1);}												
					| REALLIT											{$$ = novoNo("RealLit",$1);}													
					| Id 												{$$ = $1;}						
					| Id LBRAC Expr ParamList RBRAC						{$$ = novoNo("Call",""); $$->filho = $1; $1->irmao = $3; $3->irmao = $4;}
;

Not 				: NOT      											{$$ = novoNo("Not",$1);}

ParamList 			: 													{$$ = NULL;}
					| COMMA Expr ParamList 								{$$ = $2; $2->irmao = $3;}
;

Id 					: ID 												{$$ = novoNo("Id",$1);}			
;

%%

	int main(int argc, char **argv)
	{
		if(yyparse() == 0)
		{
			if (argc == 2)
			{
				if (strcmp(argv[1],"-t") == 0)
					imprimeNo(arvore,0);

				else if(strcmp(argv[1],"-s") == 0)
				{
					init();
					analisaNo(arvore,symtab);
					imprimeTabelas();
				}
			}
			else if (argc == 3)
			{
				if ((strcmp(argv[1],"-t") == 0 && strcmp(argv[2],"-s") == 0) || (strcmp(argv[1],"-s") == 0 && strcmp(argv[2],"-t") == 0))
				{
					imprimeNo(arvore,0);
					printf("\n");
					init();
					analisaNo(arvore,symtab);
					imprimeTabelas();
				}
			}

			else if (argc == 1)
			{
				init();
				analisaNo(arvore,symtab);
				geraCodigo(arvore,symtab->next->next);
			}


		}

		return 0;

	}

	int yyerror (char *s) 
	{
		printf ("Line %d, col %d: %s: %s\n", yylineno, pos_col-((int)strlen(yytext)), s, yytext);

		return 1;
	}





numero							[0-9]+
exp 							(e|E)("+"|"-")?{numero}
variavel						([a-z]|[A-Z])([0-9]|[a-z]|[A-Z])*
expressao						'[^\n']*(''[^\n']*)*

%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "y.tab.h"
	#define YY_USER_ACTION pos_col+=yyleng;
	int pos_col = 1, coment_lin, coment_col;
%}

%option yylineno caseless
%X COMMENT

%%

{numero}						{yylval.str=(char*)strdup(yytext);return INTLIT;}

{numero}"."{numero}{exp}		{yylval.str=(char*)strdup(yytext);return REALLIT;}

{numero}"."{numero}				{yylval.str=(char*)strdup(yytext);return REALLIT;}
	
{numero}{exp}					{yylval.str=(char*)strdup(yytext);return REALLIT;}

{expressao}'('')*				{yylval.str=(char*)strdup(yytext);return STRING;}

{expressao}'+					{printf("Line %d, col %d: unterminated string\n",yylineno,pos_col-yyleng);}

{expressao}"\n"					{printf("Line %d, col %d: unterminated string\n",yylineno-1,pos_col-yyleng);
								pos_col=1;
								}

{expressao}						{printf("Line %d, col %d: unterminated string\n",yylineno,pos_col-yyleng);}

"and"							{yylval.str=(char*)strdup(yytext);return AND;}
"or"							{yylval.str=(char*)strdup(yytext);return OR;}
"<"								{return LT;}
">"								{return GT;}
"<="							{return LEQ;}
">="							{return GEQ;}
"="								{return EQ;}
"<>"							{return NEQ;}
"+"								{return ADD;}
"-"								{return SUB;}
"*"								{return MUL;}
"/"								{return REALDIV;}
"mod"							{yylval.str=(char*)strdup(yytext);return MOD;}
"div"							{yylval.str=(char*)strdup(yytext);return DIV;}

"("								{return LBRAC;}
")"								{return RBRAC;}

"."								{return DOT;}
":"								{return COLON;}
","								{return COMMA;}
";"								{return SEMIC;}
":="							{return ASSIGN;}
"not"							{yylval.str=(char*)strdup(yytext);return NOT;}

"begin"							{return BEGINO;}
"do"							{return DO;}
"else"							{return ELSE;}
"end"							{return END;}
"forward"						{return FORWARD;}
"function"						{return FUNCTION;}
"if"							{return IF;}
"output"						{return OUTPUT;}
"paramstr"						{return PARAMSTR;}
"program"						{return PROGRAM;}
"repeat"						{return REPEAT;}
"then"							{return THEN;}
"until"							{return UNTIL;}
"val"							{return VAL;}
"var"							{return VAR;}
"while"							{return WHILE;}
"writeln"						{return WRITELN;}


"abs"|"arctan"|"array"|"case"|"char"|"chr"|"const"|"cos"|"dispose"					{return RESERVED;}
"downto"|"eof"|"eoln"|"exp"|"file"|"for"|"get"|"goto"|"in"							{return RESERVED;}
"input"|"label"|"ln"|"maxint"|"new"|"nil"|"odd"|"of"								{return RESERVED;}
"ord"|"pack"|"packed"|"page"|"pred"|"procedure"|"put"|"read"						{return RESERVED;}
"readln"|"record"|"reset"|"rewrite"|"round"|"set"|"sin"|"sqr"						{return RESERVED;}
"sqrt"|"succ"|"text"|"to"|"trunc"|"type"|"unpack"|"with"|"write"					{return RESERVED;}

{variavel}						{yylval.str=(char*)strdup(yytext);return ID;}

"\n"							{pos_col=1;}
"\t"							;
" "								;
"(*"							{BEGIN COMMENT;
								coment_lin=yylineno;
								coment_col=pos_col-2;
								}
"{"								{BEGIN COMMENT;
								coment_lin=yylineno;
								coment_col=pos_col-1;
								}

<COMMENT>"*)"|"}"				{BEGIN 0;}

<COMMENT>"\n"					{pos_col=1;}
<COMMENT>.						;
<COMMENT><<EOF>>				{printf("Line %d, col %d: unterminated comment\n",coment_lin,coment_col);
								yyterminate();}

.								{printf("Line %d, col %d: illegal character ('%c')\n",yylineno,pos_col-1,yytext[0]);}

%%

int yywrap()
{
	return 1;
}
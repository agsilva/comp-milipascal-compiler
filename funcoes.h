#include "tabela_sym.h"

char* toLower(char* str);

void addFunc(char* name);
Tabela* getFunc(char* name);

Elem *getElem(No *no, Tabela *tabela);
Elem *verifElem(No *no, Tabela *tabela);

int getFuncNum(Tabela* tabela);
int getCallNum(No* no);

No* getVarTipo(No *no);
char* verifVarTipo(No *no, Tabela *tabela);
char* verifVarDecl(No *no, Tabela *tabela);

int verifType(char *tipo);
char *verifOp(No *no);

char *verifCall(No *callNo, Tabela *tabela);

void imprimeErro(int num, int lin, int col, char *str1, char *str2, char *str3, char *str4);

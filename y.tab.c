/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "mpacompiler.y" /* yacc.c:339  */


	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "arvore.h"
	#include "tabela_sym.h"

	extern int yylineno, yyleng;
	extern int pos_col;
	extern char* yytext;

	No* arvore = NULL;
	Tabela *symtab = NULL;

	int yyerror(char *s);

#line 84 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    INTLIT = 258,
    REALLIT = 259,
    STRING = 260,
    DOT = 261,
    COLON = 262,
    COMMA = 263,
    SEMIC = 264,
    ASSIGN = 265,
    BEGINO = 266,
    DO = 267,
    ELSE = 268,
    END = 269,
    FORWARD = 270,
    FUNCTION = 271,
    ID = 272,
    IF = 273,
    OUTPUT = 274,
    PARAMSTR = 275,
    PROGRAM = 276,
    REPEAT = 277,
    UNTIL = 278,
    VAL = 279,
    VAR = 280,
    WHILE = 281,
    WRITELN = 282,
    RESERVED = 283,
    THEN = 284,
    LT = 285,
    LEQ = 286,
    GT = 287,
    GEQ = 288,
    EQ = 289,
    NEQ = 290,
    ADD = 291,
    SUB = 292,
    MUL = 293,
    REALDIV = 294,
    OR = 295,
    MOD = 296,
    DIV = 297,
    AND = 298,
    NOT = 299,
    LBRAC = 300,
    RBRAC = 301
  };
#endif
/* Tokens.  */
#define INTLIT 258
#define REALLIT 259
#define STRING 260
#define DOT 261
#define COLON 262
#define COMMA 263
#define SEMIC 264
#define ASSIGN 265
#define BEGINO 266
#define DO 267
#define ELSE 268
#define END 269
#define FORWARD 270
#define FUNCTION 271
#define ID 272
#define IF 273
#define OUTPUT 274
#define PARAMSTR 275
#define PROGRAM 276
#define REPEAT 277
#define UNTIL 278
#define VAL 279
#define VAR 280
#define WHILE 281
#define WRITELN 282
#define RESERVED 283
#define THEN 284
#define LT 285
#define LEQ 286
#define GT 287
#define GEQ 288
#define EQ 289
#define NEQ 290
#define ADD 291
#define SUB 292
#define MUL 293
#define REALDIV 294
#define OR 295
#define MOD 296
#define DIV 297
#define AND 298
#define NOT 299
#define LBRAC 300
#define RBRAC 301

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 19 "mpacompiler.y" /* yacc.c:355  */

	char* str;
	struct no* no;

#line 221 "y.tab.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 236 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  6
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   145

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  47
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  32
/* YYNRULES -- Number of rules.  */
#define YYNRULES  75
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  147

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   301

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    67,    67,    70,    73,    76,    77,    80,    81,    84,
      87,    88,    91,    92,    93,    94,    97,    98,   101,   104,
     105,   108,   109,   112,   115,   118,   121,   122,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   137,   140,
     141,   144,   145,   148,   149,   152,   153,   154,   155,   156,
     157,   160,   161,   162,   165,   166,   168,   169,   170,   172,
     173,   176,   177,   178,   179,   180,   182,   183,   184,   185,
     186,   187,   190,   192,   193,   196
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "INTLIT", "REALLIT", "STRING", "DOT",
  "COLON", "COMMA", "SEMIC", "ASSIGN", "BEGINO", "DO", "ELSE", "END",
  "FORWARD", "FUNCTION", "ID", "IF", "OUTPUT", "PARAMSTR", "PROGRAM",
  "REPEAT", "UNTIL", "VAL", "VAR", "WHILE", "WRITELN", "RESERVED", "THEN",
  "LT", "LEQ", "GT", "GEQ", "EQ", "NEQ", "ADD", "SUB", "MUL", "REALDIV",
  "OR", "MOD", "DIV", "AND", "NOT", "LBRAC", "RBRAC", "$accept", "Prog",
  "ProHeading", "ProgBlock", "VarPart", "OptVarDecl", "VarDeclaration",
  "IDList", "FuncPart", "FuncHeading", "FormalParamList",
  "OptFormalParams", "FormalParams", "FuncBlock", "StatPart", "StatList",
  "OptStat", "Stat", "WritelnPList", "ExpStr", "OptPList", "Expr", "RelOp",
  "SimpleExpr", "Sign", "AddOp", "Term", "MultOp", "Factor", "Not",
  "ParamList", "Id", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301
};
# endif

#define YYPACT_NINF -72

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-72)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
     -12,    -7,    15,     7,   -72,   -17,   -72,     5,    13,    -7,
      29,    53,    17,   -72,    50,    66,    68,   -72,    -7,    76,
      70,   -72,    -7,    -7,    -7,    20,    40,   -72,    19,   -72,
     -72,   -72,    -7,     5,    43,    86,     9,    40,    51,     9,
      52,   -72,    94,   101,   102,   104,    76,   105,   -72,   106,
      -7,   -72,   107,    -7,   -72,   -72,   -72,   -72,   -72,     9,
      82,    69,     3,    47,   -72,     3,    72,    95,    99,   108,
      38,   -72,   -72,    40,   -72,     9,    53,   -72,    53,    53,
     -72,    43,    75,   -72,    77,    40,   -72,   -72,   -72,   -72,
     -72,   -72,   -72,   -72,   -72,     9,     3,    47,   -72,   -72,
     -72,   -72,   -72,     3,   -72,     9,     9,    79,    40,   -72,
     114,   -72,   101,   -72,   -72,   -72,   -72,   107,   -72,   -72,
     112,    41,    47,   -72,   118,   -72,     9,   -72,    38,    81,
     -72,   -72,    40,     9,    83,    84,   114,   -72,   -72,   118,
     -72,   120,   -72,   -72,    -7,    87,   -72
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     0,     0,     0,    75,     0,     1,     5,     0,     0,
       0,    12,     0,     6,     0,     0,    10,     2,     0,     0,
       0,     3,     7,     0,     0,     0,    28,     4,     5,     8,
       9,    11,     0,     5,     0,     0,     0,    28,     0,     0,
      36,    29,     0,    26,     0,     0,     0,     0,    17,     0,
       0,    21,    19,     0,    68,    69,    54,    55,    72,     0,
       0,    43,     0,    51,    59,     0,    70,     0,     0,     0,
       0,    37,    24,    28,    25,     0,    12,    23,    12,    12,
      22,     0,     0,    16,     0,    28,    45,    46,    47,    48,
      49,    50,    56,    57,    58,     0,     0,    52,    65,    63,
      64,    62,    61,     0,    66,     0,     0,     0,    28,    40,
      41,    39,    26,    35,    13,    14,    15,    19,    18,    67,
      30,    44,    53,    60,    73,    33,     0,    32,     0,     0,
      27,    20,    28,     0,     0,     0,    41,    38,    31,    73,
      71,     0,    42,    74,     0,     0,    34
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -72,   -72,   -72,   -72,   125,   113,   -31,   110,    16,   -72,
     -72,    21,    55,   109,    -8,   100,    27,   -71,   -72,    12,
       8,   -35,   -72,    46,   -72,   -72,   -57,   -72,   -47,   -72,
       6,    -1
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,    10,    46,    13,    14,    15,    19,    20,
      35,    82,    52,    47,    41,    42,    74,    43,    71,   110,
     129,   111,    95,    61,    62,    96,    63,   103,    64,    65,
     134,    66
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
       5,    60,   112,    51,    69,    97,    54,    55,    16,     1,
       4,    27,    54,    55,   120,     6,     7,    25,   104,    80,
       4,    16,    30,    16,    84,    44,     4,    32,     8,    33,
       9,    48,    12,    16,    45,    17,    44,   127,    77,   122,
     113,    54,    55,   109,     9,    56,    57,    58,    59,    16,
      51,    26,    83,    58,    59,     4,   123,     4,    36,    22,
       4,   138,    37,    21,    38,    34,    39,    40,    50,    18,
     124,   125,    44,    23,    56,    57,    24,    92,    93,    28,
      16,    94,    58,    59,    44,    98,    99,    26,   100,   101,
     102,   135,   114,    53,   115,   116,    68,    70,   139,    86,
      87,    88,    89,    90,    91,    92,    93,    44,    72,    94,
      73,    85,    75,    76,    78,    79,    81,   105,   106,   107,
     108,   118,   128,   119,   126,   132,   133,   137,   144,   140,
     141,    44,    11,   146,    31,    29,   117,    67,   131,   130,
     136,   121,    49,   145,   142,   143
};

static const yytype_uint8 yycheck[] =
{
       1,    36,    73,    34,    39,    62,     3,     4,     9,    21,
      17,    19,     3,     4,    85,     0,     9,    18,    65,    50,
      17,    22,    23,    24,    59,    26,    17,     7,    45,     9,
      25,    32,    19,    34,    15,     6,    37,   108,    46,    96,
      75,     3,     4,     5,    25,    36,    37,    44,    45,    50,
      81,    11,    53,    44,    45,    17,   103,    17,    18,     9,
      17,   132,    22,    46,    24,    45,    26,    27,    25,    16,
     105,   106,    73,     7,    36,    37,     8,    36,    37,     9,
      81,    40,    44,    45,    85,    38,    39,    11,    41,    42,
      43,   126,    76,     7,    78,    79,    45,    45,   133,    30,
      31,    32,    33,    34,    35,    36,    37,   108,    14,    40,
       9,    29,    10,     9,     9,     9,     9,    45,    23,    20,
      12,    46,     8,    46,    45,    13,     8,    46,     8,    46,
      46,   132,     7,    46,    24,    22,    81,    37,   117,   112,
     128,    95,    33,   144,   136,   139
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    21,    48,    49,    17,    78,     0,     9,    45,    25,
      50,    51,    19,    52,    53,    54,    78,     6,    16,    55,
      56,    46,     9,     7,     8,    78,    11,    61,     9,    52,
      78,    54,     7,     9,    45,    57,    18,    22,    24,    26,
      27,    61,    62,    64,    78,    15,    51,    60,    78,    60,
      25,    53,    59,     7,     3,     4,    36,    37,    44,    45,
      68,    70,    71,    73,    75,    76,    78,    62,    45,    68,
      45,    65,    14,     9,    63,    10,     9,    61,     9,     9,
      53,     9,    58,    78,    68,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    40,    69,    72,    73,    38,    39,
      41,    42,    43,    74,    75,    45,    23,    20,    12,     5,
      66,    68,    64,    68,    55,    55,    55,    59,    46,    46,
      64,    70,    73,    75,    68,    68,    45,    64,     8,    67,
      63,    58,    13,     8,    77,    68,    66,    46,    64,    68,
      46,    46,    67,    77,     8,    78,    46
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    47,    48,    49,    50,    51,    51,    52,    52,    53,
      54,    54,    55,    55,    55,    55,    56,    56,    57,    58,
      58,    59,    59,    60,    61,    62,    63,    63,    64,    64,
      64,    64,    64,    64,    64,    64,    64,    64,    65,    66,
      66,    67,    67,    68,    68,    69,    69,    69,    69,    69,
      69,    70,    70,    70,    71,    71,    72,    72,    72,    73,
      73,    74,    74,    74,    74,    74,    75,    75,    75,    75,
      75,    75,    76,    77,    77,    78
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     4,     5,     3,     0,     2,     2,     3,     3,
       1,     3,     0,     5,     5,     6,     5,     4,     4,     0,
       3,     1,     2,     2,     3,     2,     0,     3,     0,     1,
       4,     6,     4,     4,     9,     3,     1,     2,     4,     1,
       1,     0,     3,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     2,     3,     1,     1,     1,     1,     1,     1,
       3,     1,     1,     1,     1,     1,     2,     3,     1,     1,
       1,     5,     1,     0,     3,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 67 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = arvore = novoNo("Program",""); (yyval.no)->filho = (yyvsp[-3].no); (yyvsp[-3].no)->irmao = (yyvsp[-1].no);}
#line 1417 "y.tab.c" /* yacc.c:1646  */
    break;

  case 3:
#line 70 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-3].no);}
#line 1423 "y.tab.c" /* yacc.c:1646  */
    break;

  case 4:
#line 73 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("VarPart",""); (yyval.no)->filho = (yyvsp[-2].no); (yyval.no)->irmao = novoNo("FuncPart",""); (yyval.no)->irmao->filho = (yyvsp[-1].no); (yyval.no)->irmao->irmao = verifStat((yyvsp[0].no));}
#line 1429 "y.tab.c" /* yacc.c:1646  */
    break;

  case 5:
#line 76 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = NULL;}
#line 1435 "y.tab.c" /* yacc.c:1646  */
    break;

  case 6:
#line 77 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1441 "y.tab.c" /* yacc.c:1646  */
    break;

  case 7:
#line 80 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("VarDecl",""); (yyval.no)->filho = (yyvsp[-1].no);}
#line 1447 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 81 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("VarDecl",""); (yyval.no)->filho = (yyvsp[-2].no); (yyval.no)->irmao = (yyvsp[0].no);}
#line 1453 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 84 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = insereNo((yyvsp[-2].no),(yyvsp[0].no));}
#line 1459 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 87 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1465 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 88 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[0].no);}
#line 1471 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 91 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = NULL;}
#line 1477 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 92 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("FuncDecl",""); (yyval.no)->filho = (yyvsp[-4].no); (yyval.no)->irmao = (yyvsp[0].no);}
#line 1483 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 93 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("FuncDef",""); (yyval.no)->filho = insereNo((yyvsp[-4].no),(yyvsp[-2].no)); (yyval.no)->irmao = (yyvsp[0].no);}
#line 1489 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 94 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("FuncDef2",""); (yyval.no)->filho = (yyvsp[-4].no); (yyvsp[-4].no)->irmao = (yyvsp[-2].no); (yyval.no)->irmao = (yyvsp[0].no);}
#line 1495 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 97 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-3].no); (yyval.no)->irmao = novoNo("FuncParams",""); (yyval.no)->irmao->filho = (yyvsp[-2].no); (yyval.no)->irmao->irmao = (yyvsp[0].no);}
#line 1501 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 98 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-2].no); (yyval.no)->irmao = novoNo("FuncParams",""); (yyval.no)->irmao->irmao = (yyvsp[0].no);}
#line 1507 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 101 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[-1].no);}
#line 1513 "y.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 104 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no)=NULL;}
#line 1519 "y.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 105 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyvsp[-1].no)->irmao = (yyvsp[0].no);}
#line 1525 "y.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 108 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Params",""); (yyval.no)->filho = (yyvsp[0].no);}
#line 1531 "y.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 109 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("VarParams",""); (yyval.no)->filho = (yyvsp[0].no);}
#line 1537 "y.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 112 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("VarPart",""); (yyval.no)->filho = (yyvsp[-1].no); (yyval.no)->irmao = verifStat((yyvsp[0].no));}
#line 1543 "y.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 115 "mpacompiler.y" /* yacc.c:1646  */
    {if ((yyvsp[-1].no) && (yyvsp[-1].no)->irmao) {(yyval.no) = novoNo("StatList",""); (yyval.no)->filho = (yyvsp[-1].no);} else (yyval.no) = (yyvsp[-1].no);}
#line 1549 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 118 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no) = insereNo((yyvsp[-1].no),(yyvsp[0].no));}
#line 1555 "y.tab.c" /* yacc.c:1646  */
    break;

  case 26:
#line 121 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = NULL;}
#line 1561 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 122 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no) = insereNo((yyvsp[-1].no),(yyvsp[0].no));}
#line 1567 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 125 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = NULL;}
#line 1573 "y.tab.c" /* yacc.c:1646  */
    break;

  case 29:
#line 126 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1579 "y.tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 127 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("IfElse",""); (yyval.no)->filho = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = verifStat((yyvsp[0].no)); (yyvsp[-2].no)->irmao->irmao = novoNo("StatList","");}
#line 1585 "y.tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 128 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("IfElse",""); (yyval.no)->filho = (yyvsp[-4].no); (yyvsp[-4].no)->irmao = verifStat((yyvsp[-2].no)); (yyvsp[-4].no)->irmao->irmao = verifStat((yyvsp[0].no));}
#line 1591 "y.tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 129 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("While",""); (yyval.no)->filho = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = verifStat((yyvsp[0].no));}
#line 1597 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 130 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Repeat",""); (yyval.no)->filho = verifStat((yyvsp[-2].no)); (yyval.no)->filho->irmao = (yyvsp[0].no);}
#line 1603 "y.tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 131 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("ValParam",""); (yyval.no)->filho = (yyvsp[-4].no); (yyvsp[-4].no)->irmao = (yyvsp[-1].no);}
#line 1609 "y.tab.c" /* yacc.c:1646  */
    break;

  case 35:
#line 132 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Assign",""); (yyval.no)->filho = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[0].no);}
#line 1615 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 133 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("WriteLn","");}
#line 1621 "y.tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 134 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("WriteLn",""); (yyval.no)->filho = (yyvsp[0].no);}
#line 1627 "y.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 137 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-2].no); (yyval.no)->irmao = (yyvsp[-1].no);}
#line 1633 "y.tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 140 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1639 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 141 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("String",(yyvsp[0].str));}
#line 1645 "y.tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 144 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = NULL;}
#line 1651 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 145 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no)->irmao = (yyvsp[0].no);}
#line 1657 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 148 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1663 "y.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 149 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no)->filho = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[0].no);}
#line 1669 "y.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 152 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Lt","");}
#line 1675 "y.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 153 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Leq","");}
#line 1681 "y.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 154 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Gt","");}
#line 1687 "y.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 155 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Geq","");}
#line 1693 "y.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 156 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Eq","");}
#line 1699 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 157 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Neq","");}
#line 1705 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 160 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1711 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 161 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no)->filho = (yyvsp[0].no);}
#line 1717 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 162 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no)->filho = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[0].no);}
#line 1723 "y.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 165 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Plus","");}
#line 1729 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 166 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Minus","");}
#line 1735 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 168 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Add","");}
#line 1741 "y.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 169 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Sub","");}
#line 1747 "y.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 170 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Or",(yyvsp[0].str));}
#line 1753 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 172 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1759 "y.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 173 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyval.no)->filho = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[0].no);}
#line 1765 "y.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 176 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("And",(yyvsp[0].str));}
#line 1771 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 177 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Div",(yyvsp[0].str));}
#line 1777 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 178 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("RealDiv","");}
#line 1783 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 179 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Mod",(yyvsp[0].str));}
#line 1789 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 180 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Mul","");}
#line 1795 "y.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 182 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyvsp[-1].no)->filho = (yyvsp[0].no);}
#line 1801 "y.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 183 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no);}
#line 1807 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 184 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("IntLit",(yyvsp[0].str));}
#line 1813 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 185 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("RealLit",(yyvsp[0].str));}
#line 1819 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 186 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[0].no);}
#line 1825 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 187 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Call",""); (yyval.no)->filho = (yyvsp[-4].no); (yyvsp[-4].no)->irmao = (yyvsp[-2].no); (yyvsp[-2].no)->irmao = (yyvsp[-1].no);}
#line 1831 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 190 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Not",(yyvsp[0].str));}
#line 1837 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 192 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = NULL;}
#line 1843 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 193 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = (yyvsp[-1].no); (yyvsp[-1].no)->irmao = (yyvsp[0].no);}
#line 1849 "y.tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 196 "mpacompiler.y" /* yacc.c:1646  */
    {(yyval.no) = novoNo("Id",(yyvsp[0].str));}
#line 1855 "y.tab.c" /* yacc.c:1646  */
    break;


#line 1859 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 199 "mpacompiler.y" /* yacc.c:1906  */


	int main(int argc, char **argv)
	{
		if(yyparse() == 0)
		{
			if (argc == 2)
			{
				if (strcmp(argv[1],"-t") == 0)
					imprimeNo(arvore,0);

				else if(strcmp(argv[1],"-s") == 0)
				{
					init();
					analisaNo(arvore,symtab);
					imprimeTabelas();
				}
			}
			else if (argc == 3)
			{
				if ((strcmp(argv[1],"-t") == 0 && strcmp(argv[2],"-s") == 0) || (strcmp(argv[1],"-s") == 0 && strcmp(argv[2],"-t") == 0))
				{
					imprimeNo(arvore,0);
					printf("\n");
					init();
					analisaNo(arvore,symtab);
					imprimeTabelas();
				}
			}

			else if (argc == 1)
			{
				init();
				analisaNo(arvore,symtab);
				geraCodigo(arvore,symtab->next->next);
			}


		}

		return 0;

	}

	int yyerror (char *s) 
	{
		printf ("Line %d, col %d: %s: %s\n", yylineno, pos_col-((int)strlen(yytext)), s, yytext);

		return 1;
	}





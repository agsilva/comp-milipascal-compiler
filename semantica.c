#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "semantica.h"
#include "funcoes.h"

void analisaNo(No *no, Tabela *tabela)
{
	Tabela *tab;

	while(no)
	{
		if (strcmp(no->tipo,"Program") == 0)
		{
			tab = insereTabela("Program Symbol Table");
			analisaNo(no->filho, tab);
		}
		else if (strcmp(no->tipo,"VarPart") == 0)
			analisaNo(no->filho, tabela);
			
		else if (strcmp(no->tipo,"VarDecl") == 0)
			verifVarPart(no->filho, NULL, tabela);

		else if (strcmp(no->tipo,"Params") == 0)
			verifVarPart(no->filho,"param", tabela);
			
		else if (strcmp(no->tipo,"VarParams") == 0)
			verifVarPart(no->filho,"varparam", tabela);
			
		else if (strcmp(no->tipo,"FuncPart") == 0)
		{
			verifFuncPart(no->filho);
			verifStatPart(no->irmao,tabela);
		}
		
		no = no->irmao;
	}
}

void verifVarPart(No *no, char* varTipo, Tabela *tabela)
{
    char* tipo;
    No *aux;
    
    if (!no)
		return;
		
	if (strcmp(no->tipo,"Id") == 0)
	{
		aux = getVarTipo(no);
		
		tipo = verifVarTipo(aux,tabela);
		
		while(no->irmao)
		{
			insereElem(tabela, no, NULL, tipo, varTipo, NULL);
			no = no->irmao;
		}
	}      
}

void verifFuncPart(No *no)
{
    Tabela *tab;
    char *tipo;
    Elem *el;
    
	while(no)
	{
		if (strcmp(no->tipo,"FuncDef2") == 0)
		{
			tab = getFunc(no->filho->texto);

			if (tab){
				analisaNo(no->filho->irmao, tab);
				verifStatPart(no->filho->irmao->irmao, tab);
			}
			else
			{
				getElem(no->filho,NULL);
				/*2 - Function identifier expected*/
				imprimeErro(2,no->filho->linha,no->filho->coluna,NULL,NULL,NULL,NULL);
			}
		}
		else /* FuncDecl e FuncDef */
		{
			el = verifElem(no->filho,NULL);
			
			if (el)
			{
				/* 8 - Symbol <token> already defined */
				imprimeErro(8, no->filho->linha, no->filho->coluna, no->filho->texto, NULL, NULL, NULL);
			}	
			
			tab = insereTabela("Function Symbol Table");

			addFunc(no->filho->texto);
			tipo = verifVarTipo(no->filho->irmao->irmao, tab);
		
			insereElem(tab,no->filho,NULL, tipo, "return", NULL);
			analisaNo(no->filho->irmao->filho, tab);
			
			if (strcmp(no->tipo,"FuncDef") == 0){
				analisaNo(no->filho->irmao->irmao->irmao, tab);
				verifStatPart(no->filho->irmao->irmao->irmao->irmao, tab);
			}
		}

		no = no->irmao;
	}
}


char* verifStatPart(No *no, Tabela *tabela)
{
    char *tipoL, *tipoR, num[30];
    Tabela *tab;
    Elem *el;
    No *aux;
    int varsF;
    
	while(no)
	{ 
		if (strcmp(no->tipo,"StatList") == 0)
		{
			verifStatPart(no->filho,tabela);
		}
		else if(strcmp(no->tipo,"IfElse") == 0)
		{
			tipoL = verifStatPart(no->filho,tabela);
			if (strcmp(tipoL,"_boolean_") == 0)
			{
				verifStatPart(no->filho->irmao,tabela);
				verifStatPart(no->filho->irmao->irmao,tabela);			
			}
			else
			{
				/*5 - Incompatible type in <statement> statement (got <type>, expected <type>)*/
				imprimeErro(5,no->filho->linha,no->filho->coluna, "if", tipoL, "_boolean_",NULL);
			}
		}
		else if(strcmp(no->tipo,"While") == 0)
		{
			
			tipoL = verifStatPart(no->filho,tabela);
			if (strcmp(tipoL,"_boolean_") == 0)
			{
				verifStatPart(no->filho->irmao,tabela);	
			}
			else
			{
				/*5 - Incompatible type in <statement> statement (got <type>, expected <type>)*/
				imprimeErro(5,no->filho->linha,no->filho->coluna, "while", tipoL, "_boolean_",NULL);
			}
		}
		else if(strcmp(no->tipo,"Repeat") == 0)
		{
			tipoL = verifStatPart(no->filho->irmao,tabela);
			if (strcmp(tipoL,"_boolean_") == 0)
			{
				verifStatPart(no->filho,tabela);	
			}
			else
			{
				/*5 - Incompatible type in <statement> statement (got <type>, expected <type>)*/
				imprimeErro(5,no->filho->irmao->linha,no->filho->irmao->coluna, "repeat-until", tipoL, "_boolean_",NULL);
			}
		}
		else if(strcmp(no->tipo,"ValParam") == 0)
		{
			tipoL = verifStatPart(no->filho,tabela);

			if (strcmp(tipoL,"_integer_") == 0)
			{
				el = getElem(no->filho->irmao,tabela);
				
				if (strcmp(el->tipo, "_integer_") != 0)
					imprimeErro(5,no->filho->irmao->linha,no->filho->irmao->coluna, "val-paramstr", el->tipo, "_integer_",NULL);
			}
			else
			{
				/*5 - Incompatible type in <statement> statement (got <type>, expected <type>)*/
				imprimeErro(5,no->filho->linha,no->filho->coluna, "val-paramstr", tipoL, "_integer_",NULL);
			}
		}
		else if(strcmp(no->tipo,"Assign") == 0)		
		{
			el = getElem(no->filho,tabela);
			
			if (el->flag && strcmp(el->flag,"constant") == 0)
				imprimeErro(11,no->filho->linha,no->filho->coluna,NULL,NULL,NULL,NULL); /*	11 - Variable identifier expected */


			if (verifType(el->tipo) == 0)
				imprimeErro(11,no->filho->linha,no->filho->coluna,NULL,NULL,NULL,NULL); /*	11 - Variable identifier expected */
				
			
			tipoL = el->tipo;
				
			tipoR = verifStatPart(no->filho->irmao,tabela);
			
			if (!(strcmp(tipoL,"_integer_") == 0 && strcmp(tipoR,"_integer_") == 0)
				&& !(strcmp(tipoL,"_real_") == 0 && (strcmp(tipoR,"_integer_") == 0 || strcmp(tipoR,"_real_") == 0)) 
				&& !(strcmp(tipoL,"_boolean_") == 0 && strcmp(tipoR,"_boolean_") == 0)) 
			{  
				/* 4 - Incompatible type in assigment to <token> (got <type>, expected <type>) */
				imprimeErro(4,no->filho->irmao->linha,no->filho->irmao->coluna, no->filho->texto, tipoR, tipoL, NULL);
			}
		}
			
		else if(strcmp(no->tipo,"WriteLn") == 0)
		{
			aux = no->filho;
			while(aux)
			{
				if(strcmp(aux->tipo,"String") != 0)
				{					
					tipoL = verifStatPart(aux,tabela);
					
					if (verifType(tipoL) == 0)
						imprimeErro(1, aux->linha, aux->coluna, tipoL, NULL, NULL, NULL); /* 1 - Cannot write values of type <type> */
				}	
				
				aux = aux->irmao;
			}
		}
		
		/*** EXPRESSOES ***/
		else if (strcmp(no->tipo,"Lt") == 0 || strcmp(no->tipo,"Leq") == 0 || strcmp(no->tipo,"Gt") == 0 || strcmp(no->tipo,"Geq") == 0 || strcmp(no->tipo,"Eq") == 0 || strcmp(no->tipo,"Neq") == 0)
		{
			tipoL = verifStatPart(no->filho,tabela);
			tipoR = verifStatPart(no->filho->irmao,tabela);
			
			if ((strcmp(tipoL,"_integer_") == 0 || strcmp(tipoL,"_real_") == 0) && (strcmp(tipoR,"_integer_") == 0 || strcmp(tipoR,"_real_") == 0))
				return "_boolean_";
				
			if (strcmp(tipoL,"_boolean_") == 0 && strcmp(tipoR,"_boolean_") == 0)
				return "_boolean_";
	
			/* 7 - Operator <token> cannot be applied to types <type>, <type> */
			imprimeErro(7, no->linha,no->coluna, verifOp(no), tipoL, tipoR, NULL);
		}
				
		else if (strcmp(no->tipo,"Add") == 0 || strcmp(no->tipo,"Sub") == 0 || strcmp(no->tipo,"Mul") == 0)
		{
			tipoL = verifStatPart(no->filho,tabela);
			tipoR = verifStatPart(no->filho->irmao,tabela);
			
			if (strcmp(tipoL,"_integer_") == 0 && strcmp(tipoR,"_integer_") == 0) 
				return "_integer_";
				
			if ((strcmp(tipoL,"_integer_") == 0 || strcmp(tipoL,"_real_") == 0) && (strcmp(tipoR,"_integer_") == 0 || strcmp(tipoR,"_real_") == 0))
				return "_real_";
	
			/* 7 - Operator <token> cannot be applied to types <type>, <type> */
			imprimeErro(7, no->linha,no->coluna, verifOp(no), tipoL, tipoR, NULL);
		}
		
		else if (strcmp(no->tipo,"RealDiv") == 0)
		{
			tipoL = verifStatPart(no->filho,tabela);
			tipoR = verifStatPart(no->filho->irmao,tabela);
			
			if ((strcmp(tipoL,"_integer_") == 0 || strcmp(tipoL,"_real_") == 0) && (strcmp(tipoR,"_integer_") == 0 || strcmp(tipoR,"_real_") == 0))
				return "_real_";
	
			/* 7 - Operator <token> cannot be applied to types <type>, <type> */
			imprimeErro(7, no->linha,no->coluna, verifOp(no), tipoL, tipoR, NULL);
		}
		
		else if (strcmp(no->tipo,"Div") == 0 || strcmp(no->tipo,"Mod") == 0)
		{
			tipoL = verifStatPart(no->filho,tabela);
			tipoR = verifStatPart(no->filho->irmao,tabela);
			
			if (strcmp(tipoL,"_integer_") == 0 && strcmp(tipoR,"_integer_") == 0)
				return "_integer_";
				
			/* 7 - Operator <token> cannot be applied to types <type>, <type> */
			imprimeErro(7, no->linha,no->coluna, verifOp(no), tipoL, tipoR, NULL);
		}
		
		else if (strcmp(no->tipo,"And") == 0 || strcmp(no->tipo,"Or") == 0){
			tipoL = verifStatPart(no->filho,tabela);
			tipoR = verifStatPart(no->filho->irmao,tabela);
			
			if (strcmp(tipoL,"_boolean_") == 0 && strcmp(tipoR,"_boolean_") == 0)
				return "_boolean_";
			
			/* 7 - Operator <token> cannot be applied to types <type>, <type> */
			imprimeErro(7, no->linha,no->coluna, verifOp(no), tipoL, tipoR, NULL);
		}
		
		else if (strcmp(no->tipo,"Plus") == 0 || strcmp(no->tipo,"Minus") == 0)
		{
			tipoR = verifStatPart(no->filho,tabela);
			
			if (strcmp(tipoR,"_integer_") == 0 || strcmp(tipoR,"_real_") == 0)
				return tipoR;
				
			/* 6 - Operator <token> cannot be applied to type <type> */	
			imprimeErro(6, no->linha,no->coluna, verifOp(no), tipoR, NULL,NULL);
		}
		
		else if (strcmp(no->tipo,"Not") == 0 )
		{
			tipoR = verifStatPart(no->filho,tabela);
			
			if (strcmp(tipoR,"_boolean_") == 0)
				return "_boolean_";
			
			/* 6 - Operator <token> cannot be applied to type <type> */	
			imprimeErro(6, no->linha,no->coluna, verifOp(no), tipoR, NULL,NULL);
		}
		
		else if (strcmp(no->tipo,"IntLit") == 0 )
			return "_integer_";
			
		else if (strcmp(no->tipo,"RealLit") == 0 )
			return "_real_";
					
		else if (strcmp(no->tipo,"Id") == 0 )
		{
			el = getElem(no, tabela);

			if(strcmp(el->tipo, "_function_") == 0 || (el->flag && strcmp(el->flag,"return") == 0))
			{
				tab = getFunc(el->nome);
				
				if(strcmp(el->nome, "paramcount") == 0 && !tab) 
					return "_integer_";
			
				if((varsF = getFuncNum(tab)) == 0)
					return tab->elem->tipo;

				sprintf(num, "%d", varsF);

				/* 12 - Wrong number of arguments in call to function <token> (got <number>, expected <number>) */
				imprimeErro(12, no->linha, no->coluna, no->texto, "0", num, NULL);	
			}
			
			return el->tipo;
		}
				
		else if (strcmp(no->tipo,"Call") == 0){
			
			return verifCall(no, tabela);
		}
			
		no = no->irmao;
	}
	
	return NULL;
}

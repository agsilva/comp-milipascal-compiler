#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "semantica.h"
#include "funcoes.h"

extern Tabela *symtab;

void init(void)
{
	Tabela *tab;

	symtab = novaTabela("Outer Symbol Table");
	insereElem(symtab, NULL, "boolean", "_type_", "constant", "_boolean_");
	insereElem(symtab, NULL, "integer", "_type_", "constant", "_integer_");
	insereElem(symtab, NULL, "real", "_type_", "constant", "_real_");
	insereElem(symtab, NULL, "false", "_boolean_", "constant", "_false_");
	insereElem(symtab, NULL, "true", "_boolean_", "constant", "_true_");
	insereElem(symtab, NULL, "paramcount", "_function_", NULL, NULL);
	insereElem(symtab, NULL, "program", "_program_", NULL, NULL);
	tab = insereTabela("Function Symbol Table");
	insereElem(tab, NULL, "paramcount", "_integer_", "return", NULL);
}

void imprimeTabelas(void)
{
	Elem *el;
	Tabela *tab = symtab;
	
	while(tab)
	{
		printf("===== %s =====\n", tab->titulo);
		el = tab->elem;
		
		while(el)
		{
			printf("%s\t%s", el->nome, el->tipo);
			if (el->flag)
			{
				printf("\t%s", el->flag);
				if (el->valor)
					printf("\t%s", el->valor);
			}
			printf("\n");
			el = el->next;
		}
		
		if (tab->next) 
			printf("\n");
			
		tab = tab->next;
	}
}

Tabela* novaTabela(char *titulo)
{
	Tabela* nova = (Tabela*)malloc(sizeof(Tabela));
	
	nova->titulo = titulo;
	nova->elem = NULL;
	nova->next = NULL;
	
	return nova;
}

Tabela* insereTabela(char* titulo)
{
	Tabela* nova = symtab;
	
	while (nova->next)
		nova = nova->next;
		
	nova->next = novaTabela(titulo);
	
	return nova->next;
}

Elem* novoElem(char *nome, char *tipo, char *flag, char *valor)
{
	Elem* novo = (Elem*)malloc(sizeof(Elem));
	
	novo->nome = nome;
	novo->tipo = tipo;
	novo->flag = flag;
	novo->valor = valor;
	novo->next = NULL;
	
	return novo;
}

void insereElem(Tabela* tabela, No* no, char* nome, char* tipo ,char* flag, char* valor)
{
	Elem *ant, *aux;
	char *str;
	
	if (no)
		str = toLower(no->texto);
	else
		str = toLower(nome);

	if ((aux = tabela->elem))
	{	
		for(; aux != NULL; ant = aux, aux = aux->next)
			if(strcmp(aux->nome, str) == 0)
				imprimeErro(8,no->linha,no->coluna,no->texto,NULL,NULL,NULL); /*Symbol already defined*/
			
		
		ant->next = novoElem(str,tipo,flag,valor);
	}
	
	else	
		tabela->elem = novoElem(str,tipo,flag,valor);
		
}



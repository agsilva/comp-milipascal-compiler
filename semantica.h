#include "tabela_sym.h"

/**** ANALISA ARVORE DE SINTAXE ****/
void analisaNo(No *no, Tabela *tabela);
void verifVarPart(No *no, char* var_type, Tabela *tabela);
void verifFuncPart(No *no);
char* verifStatPart(No *no, Tabela *tabela);


#ifndef ARVORE_H
#define ARVORE_H

typedef struct no{
	char* tipo;
	char* texto;
   	int linha;
    	int coluna;
	struct no *irmao;
	struct no *filho;
}No;

typedef struct elem{
	char* nome;
    char* tipo;
	char* flag;
    char* valor;
	struct elem *next;
}Elem;

typedef struct tabela{
    char* titulo;
    Elem *elem;
    struct tabela *next;
}Tabela;

typedef struct codigo{
	char *linha;
	int id;
	struct codigo *next;	
}Codigo;

typedef struct valorBool{
	int valor;
	char *nome;
	char *function;
	struct valorBool *next;
}ValorBool;

No* novoNo(char* tipo, char* texto);
No* verifStat(No* no);
No* insereNo(No* ini, No* fin);
void imprimePontos(int lvl);
void imprimeNo(No* no, int lvl);

#endif
